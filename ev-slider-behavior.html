<link rel="import" href="../polymer/polymer.html">
<script>
  /**
   * `EVSliderBehavior` provides a behavior which utilises SVG animations
   * and HTML5 Audio to emulate a traditional MP4 video.
   *
   * @polymer
   * @mixinFunction
   */
  const EVSliderBehavior = function (superClass) {
    return class extends superClass {

      static get properties() {
        return {
          /**
          * The value the slider contains.
          */
          value: { type: Number, value: 0 },

          /**
          * The value the slider can not go below. For example we can have 0 to 10 steps shown but the slider
          * can not go below the default start value of 1. Used for the restricted slider.
          */
          start: Number,

          /**
          * The value the slider can not go beyond. Used for the restricted slider.
          */
          stop: Number,

          /**
          * If true, the slider thumb snaps to tick marks that are evenly spaced, based
          * on the `step` property value.
          */
          snaps: { type: Boolean, value: false, notify: true },

          /**
          * The number of steps between min and max; must be an integer. The default is 1.
          */
          step: { type: Number, value: 1 },

          /**
          * If true, a label with a numeric value label is shown above the slider thumb.
          * Used for settings for when users need to know the exact value of the setting.
          */
          pin: { type: Boolean, value: false, notify: true },

          /**
          * The immediate value of the slider.  This value is updated while the user
          * is dragging the slider.
          */
          immediateValue: { type: Number, value: 0, readOnly: true, notify: true },

          /**
          * If this is set, markers are displayed. The maximum number of markers must be specified.
          */
          maxMarkers: { type: Number, value: 0, notify: true },

          /**
          * If true, a touchmove on the slider bar doesn't drag the slider thunb.
          * Tapping on the slider bar still updates the slider's position
          */
          ignoreBarTouch: { type: Boolean, value: false },

          /**
          * If true, no ripple effect is displayed
          */
          noRipple: { type: Boolean, value: false },

          /**
          * True when the user is dragging the slider.
          */
          dragging: { type: Boolean, value: false, readOnly: true, notify: true },

          /**
          * True when the the slider is transitioning.
          */
          transiting: { type: Boolean, value: false, readOnly: true },

          /**
          * @private
          * The markers array for the tick marks
          */
          markers: {
            type: Array,
            readOnly: true,
            value: () => {
              return [];
            }
          }
        }
      }

      /**
      * @private
      * Set up the observer functions
      * @return {Array} an array of observers to functions
      */
      static get observers() {
        return [
          '_updateKnob(value, min, max, snaps, step)',
          '_valueChanged(value)',
          '_immediateValueChanged(immediateValue)',
          '_updateMarkers(maxMarkers, min, max, snaps)'
        ]
      }

      /**
      * @private
      * Used to add the correct animation property depending on the browser
      * @return {String} returns the correct animationend event for the browser
      */
      _whichBrowserEvent() {
        let animations = {
          "animation": "animationend",
          "OAnimation": "oAnimationEnd",
          "MozAnimation": "animationend",
          "WebkitAnimation": "webkitAnimationEnd"
        }

        for (let element in animations) {
          if (this.$.ripple.style[element] !== undefined) {
            return animations[element];
          }
        }
      }

      /**
      * @private
      * Called on a mouse down event
      * @param {event} browser event
      */
      _onMouseDown(event) {
        this.$.ripple.classList.remove('focus-ripple');
        this.$.ripple.classList.remove('hover-ripple');
        this.$.ripple.classList.remove('unhover-ripple');
        this.$.ripple.classList.add('mousedown-ripple');
        this.$.ripple.classList.add('large-ripple');
        this.$.ripple.style.setProperty('opacity', 'var(--ev-hover-opacity)');

        this.$.ripple.classList.add('hover-opacity-ie11');

        let transitionEvent = this._whichBrowserEvent();
        this.$.ripple.addEventListener(transitionEvent, event => { this._mouseDownRippleFinished(event) });
      }

      /**
      * @private
      * Called when the mouse up animation ends
      */
      _mouseDownRippleFinished(event) {
        let transitionEvent = this._whichBrowserEvent();
        this.$.ripple.removeEventListener(transitionEvent, this._mouseDownRippleFinished);

        this.$.ripple.classList.remove('mouseup-ripple');
      }

      /**
      * @private
      * Called on mouse up
      * @param {event} browser event
      */
      _onFinishClick(event) {
        this.$.ripple.classList.remove('mousedown-ripple');
        this.$.ripple.classList.remove('large-ripple');
        this.$.ripple.classList.add('mouseup-ripple');
        this.$.ripple.classList.add('normal-ripple');
        this.$.ripple.style.setProperty('opacity', 'var(--ev-hover-opacity)');

        this.$.ripple.classList.add('hover-opacity-ie11');
      }

      /**
      * @private
      * Called when the user hovers over the slider thumb
      * @param {event} browser event
      */
      _onHover(event) {
        if (!this.$.sliderKnob.classList.contains('dragging')) {
          this.$.ripple.classList.remove('focus-ripple');
          this.$.ripple.classList.remove('click-ripple');
          this.$.ripple.classList.remove('unhover-ripple');
          this.$.ripple.classList.add('hover-ripple');
          this.$.ripple.style.setProperty('opacity', 'var(--ev-hover-opacity)');

          this.$.ripple.classList.add('hover-opacity-ie11');
        }
      }

      /**
      * @private
      * Called when the hovers is removed
      * @param {event} browser event
      */
      _onUnHover(event) {
        // keep focus even when mouse is moved away
        this.focus();
      }

      /**
      * @private
      * Called when the slider has focus
      * @param {event} browser event
      */
      _onFocus(event) {
        this.$.ripple.style.setProperty('opacity', 'var(--ev-focus-opacity)');

        this.$.ripple.classList.add('focus-opacity-ie11');
        this.$.ripple.classList.remove('lose-focus-opacity-ie11');
      }

      /**
      * @private
      * Called when the slider loses focus
      * @param {event} browser event
      */
      _onFocusout(event) {
        this.$.ripple.classList.remove('focus-ripple');
        this.$.ripple.classList.remove('click-ripple');
        this.$.ripple.classList.remove('hover-ripple');
        this.$.ripple.classList.remove('unhover-ripple');
        this.$.ripple.style.opacity = '0';

        this.$.ripple.classList.add('lose-focus-opacity-ie11');
      }

      /**
      * @private
      * Called when the user starts dragging the slider as we want a larger ripple
      * @param {event} browser event
      */
      _onDraggingStart(event) {
        this.$.ripple.classList.remove('normal-ripple');
        this.$.ripple.classList.add('large-ripple');
      }

      /**
      * Called when the user finishes dragging the slider
      * @param {event} browser event
      */
      _onDraggingEnd(event) {
        this.$.ripple.classList.remove('dragging-end');
        this.$.ripple.classList.remove('large-ripple');
        this.$.ripple.classList.add('normal-ripple');
        this.$.ripple.style.setProperty('opacity', 'var(--ev-hover-opacity)');

        this.$.ripple.classList.add('hover-opacity-ie11');
      }

      /**
      * Increases value by `step` but not above `max`.
      * @method increment
      */
      increment() {
        // Do not allow to use the down arrow key to go above below max or stop
        let clampValue;
        if (this.stop === undefined) {
          clampValue = this._clampValue(this.value + this.step);
        } else {
          clampValue = Math.min(this.value + this.step, this.stop);
        }

        this.value = this._clampValue(clampValue);
      }

      /**
    * Decreases value by `step` but not below `min`.
    * @method decrement
    */
      decrement() {
        // Do not allow to use the up arrow key to go below below min or start
        let clampValue;
        if (this.start === undefined) {
          clampValue = this.value - this.step;
        } else {
          clampValue = Math.max(this.value - this.step, this.start);
        }

        this.value = this._clampValue(clampValue);
      }

      /**
      * Force the slider to update the progress bar
      * Useful if the slider is hidden on initial display
      * @method initialise
      */
      initialise() {
        this._positionKnob(this._calcRatio(this.value) * 100);
      }

      /**
      * @private
      * Update the slider knob if value, min, max, snaps or step changes
      * @param {value}  the current value of the slider
      * @param {min}    the minimim value of the slider
      * @param {max}    the maximum value of the slider
      * @param {snaps}  if the slider snaps to a marker
      * @param {step}   the number of steps from min to max
      */
      _updateKnob(value, min, max, snaps, step) {
        this.setAttribute('aria-valuemin', min);
        this.setAttribute('aria-valuemax', max);
        this.setAttribute('aria-valuenow', value);

        this._positionKnob(this._calcRatio(value) * 100);
      }

      /**
      * @private
      * Fired when the value changes
      * @event value-change
      */
      _fireValueChange() {
        this.dispatchEvent(new CustomEvent('value-change'), { composed: true });
      }

      /**
      * @private
      * Fired when the value changes
      * @event immediate-value-change
      */
      _fireImmediateValueChange() {
        this.dispatchEvent(new CustomEvent('immediate-value-change'), { composed: true });
      }

      /**
      * @private
      * Called when the value changes, updates the markers
      * @event value-change
      */
      _valueChanged() {
        this._updateStepMarkers();
        this._fireValueChange();
      }

      /**
      * @private
      * Called when the slider value is changing, for example when dragging
      * @event immediate-value-change
      */
      _immediateValueChanged() {
        if (this.dragging) {
          this._fireImmediateValueChange();
        } else {
          this.value = this.immediateValue;
        }
      }

      /**
      * Calculates the sliders thumb position
      * @param ratio  the ratio of slider value to it's width
      * @return the thumb position between min and max
      */
      _calcKnobPosition(ratio) {
        return (this.max - this.min) * ratio / 100 + this.min;
      }

      /**
      * @private
      * Fired when the user starts dragging the slider
      * @event dragging-start
      */
      _fireDraggingStart() {
        this.dispatchEvent(new CustomEvent('dragging-start'), { composed: true });
      }

      /**
      * @private
      * Fired when the user starts dragging the slider
      * @event dragging
      */
      _fireDragging() {
        this.dispatchEvent(new CustomEvent('dragging'), { composed: true });
      }

      /**
      * @private
      * Fired when the user starts dragging the slider
      * @event change
      */
      _fireChange() {
        this.dispatchEvent(new CustomEvent('change'), { composed: true });
      }

      /**
      * @private
      * Fired when the user starts dragging the slider
      * @event dragging-end
      */
      _fireDraggingEnd() {
        this.dispatchEvent(new CustomEvent('dragging-end'), { composed: true });
      }

      /**
      * @private
      * Called when the user finishes dragging the slider
      * @param {event} browser event
      */
      _trackEnd() {
        const s = this.$.sliderKnob.style;

        this.$.sliderKnob.classList.remove('dragging');
        this.$.sliderKnob.classList.add('dragging-end');
        this._setDragging(false);
        this.value = this.immediateValue;

        s.transform = s.webkitTransform = '';

        this._fireChange();
        this._fireDraggingEnd();
      }

      /**
      * @private
      * Called when the user presses down on the slider thumb
      * @param {event} browser event
      */
      _knobdown(event) {
        // cancel selection
        event.preventDefault();

        // set the focus manually because we will called prevent default
        this.focus();
      }

      /**
      * @private
      * Called when the user changes the slider by dragging or touching
      * @param {event} browser event
      */
      _bartrack(event) {
        if (this._allowBarEvent(event)) {
          this._onTrack(event);
        }
      }

      /**
      * @private
      * Called on the on-down event of paper-progress
      * @param {event} browser event
      */
      _bardown(event) {
        if (this._allowBarEvent(event)) {
          this._barclick(event);
        }
      }

      /**
      * @private
      * Called after transitioning
      * @param {event} browser event
      */
      _knobTransitionEnd(event) {
        if (event.target === this.$.sliderKnob) {
          this._setTransiting(false);
        }
      }

      /**
      * @private
      * Sets up the marker array used for the marker divs
      * @param {maxMarkers} the number of markers
      * @param {min}        the minimim value of the slider
      * @param {max}        the maximum value of the slider
      * @param {snaps}      if the slider snaps to a marker
      */
      _updateMarkers(maxMarkers, min, max, snaps) {
        if (!snaps) {
          this._setMarkers([]);
        }

        let steps = Math.round((max - min) / this.step);

        if (steps > maxMarkers) {
          steps = maxMarkers;
        }

        if (steps < 0 || !isFinite(steps)) {
          steps = 0;
        }

        this._setMarkers(new Array(steps));
      }

      /**
      * @private
      * Returns a string with all classes that have the value of true
      * @param {classes}  an array of classes { className : value, ... }
      * @return {String}  a string of class names seperated by a space
      */
      _mergeClasses(classes) {
        return Object.keys(classes)
          .filter((className) => {
            return classes[className];
          }).join(' ');
      }

      /**
      * @private
      * Returns a string of the sliders classes that have the value of true
      * @return {String}  a string of class names seperated by a space
      */
      _getClassNames() {
        return this._mergeClasses({
          disabled: this.disabled,
          pin: this.pin,
          snaps: this.snaps,
          ring: this.immediateValue <= this.min,
          dragging: this.dragging,
          transiting: this.transiting,
          editable: this.editable
        });
      }

      /**
      * @private
      * Determines if we are allowed to update the slider after clicking or touching the bar
      * @param {event} browser event
      * @return {Boolean}
      */
      _allowBarEvent(event) {
        return (
          !this.ignoreBarTouch ||
          (event.detail.sourceEvent instanceof MouseEvent))
      }

      /**
      * @private
      * Increment the slider by one step or move to the end if the 'End' key is pressed
      * @param {event} browser event
      */
      _incrementKey(event) {
        if (!this.disabled) {
          if (event.detail.key === 'end') {
            this.value = (this.stop === undefined) ? this.value = this.max : this.value = Math.min(this.max, this.stop);
          } else {
            this.increment();
          }

          this._fireChange();
          event.preventDefault();
        }
      }

      /**
      * @private
      * Decrement the slider by one step or move to the beginning if the 'Home' key is pressed
      * @param {event} browser event
      */
      _decrementKey(event) {
        if (!this.disabled) {
          if (event.detail.key === 'home') {
            this.value = (this.start === undefined) ? this.value = this.min : this.value = Math.max(this.min, this.start);
          } else {
            this.decrement();
          }

          this._fireChange();
          event.preventDefault();
        }
      }

      /**
      * @private
      * Send a 'change' event when the slider value has changed
      * @param {event} browser event
      */
      _changeValue(event) {
        this.value = event.target.value;
        this._fireChange();
      }

      /**
      * @private
      * Stops the bubbling of an event after a key press
      * @param {event} browser event
      */
      _inputKeyDown(event) {
        event.stopPropagation();
      }

    }
  }
</script>